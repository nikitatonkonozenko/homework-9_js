//базове завдання
function createList(array, parent = document.body) {
    array.forEach(element => {
       const listItem = document.createElement("li")
       listItem.classList.add("list-item")
       listItem.innerHTML = `${element}`
       parent.append(listItem)
    });
}

//створення змінної для перевірки відпрацювання кастомного другого аргумента
const itemsList = document.createElement("ol")
document.body.append(itemsList)
createList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"], itemsList)

//перевірка відпрацювання дефолтного другого аргумента
createList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"])


