//ускладнене завдання
function createList(array, parent = document.body) {
    array.forEach(element => {
       if(Array.isArray(element)) {
        const itemsList = document.createElement("ul")
        itemsList.classList.add("items-list")
        element.forEach(item => {
            const listItem = document.createElement("li")
            listItem.classList.add("list-item-child")
            listItem.innerHTML = `${item}`
            itemsList.append(listItem)
        })
        parent.append(itemsList)
       } else {
        const listItem = document.createElement("li")
        listItem.classList.add("list-item")
        listItem.innerHTML = `${element}`
        parent.append(listItem)
       }
    })
}

let sec = 3;
const timer = document.createElement("div");
timer.innerHTML = `time left: ${sec}`;
document.body.append(timer);
function timeLeft() {
  sec--
  timer.innerHTML = `time left: ${sec}`;
}


setTimeout(() => {document.body.innerHTML = ""}, 3000)
createList(["Kharkiv",["Airport 1", "Airport 2"], "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnipro"])
setInterval(timeLeft, 1000)